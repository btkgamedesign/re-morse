﻿using UnityEngine;

public class Lightson : MonoBehaviour
{
    public Light newlight;
    public float lightpower;
    public GameObject existent;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        newlight.intensity = lightpower;
        existent.SetActive(true);
    }
}
